#!/usr/bin/env bash

# at what level to warn
BATTWARNLIMITPCT=${1:-50}

# which battery to monitor
BATTERY=0


# verify toolkit
for tool in id pgrep i3-msg acpi i3-nagbar; do
    command -v "${tool}" >/dev/null || { echo "*** please install ${tool}" >&2; exit 1; }
done


# try to find the i3 socket
export I3SOCK=/run/user/$(id -u)/i3/ipc-socket.$(pgrep -x i3)
if [ ! -e "$I3SOCK" ]; then
    echo "**** no sock found. Finetune path (/run/user/$(id -u)/i3/ipc-socket.$(pgrep -x i3))" >&2; exit 1
fi


BATLEVEL=$(LANG=C acpi -V | grep "Battery $BATTERY: Discharging, [0-9]*%" | sed -E 's|^.* Discharging, ||;s|%.*$||')

if [[ $BATLEVEL =~ ^[0-9]{1,}$ ]]; then
    if [ "$BATLEVEL" -le "$BATTWARNLIMITPCT" ]; then
	i3-msg exec 'i3-nagbar -m "**** Battery level: '$BATLEVEL'"'
    fi
else
    echo "no relevant match from acpi for now" >&2
fi
