* i3 battery alert

** Description

This is a simple script that will display a warning message when your battery
level reaches a set level. It is made to work with the [[https://i3wm.org/][i3 window manager]].

*** Status

It has been tested in Debian and Fedora.


** Requirements
These are the basic requirements:

: sudo apt-get install i3-wm x11-utils procps acpi

** Usage

The script defaults to battery level 50%. Add an integer as argument to override that.

First clone the project. Then you can try it manually first, with 101 as battery level:
: bash /path/to/folder/src/i3-battery-alert.sh 101

Setup a cron job:
: 0,10,20,30,40,50 * * * *  bash /path/to/folder/src/i3-battery-alert.sh

** Contributing

If you fix a bug, or add some nice functions, please consider making a pull request.
